import logo from './logo.svg';
import './App.css';
import ShoeShop from './ShoeShop/ShoeShop'
import FormApp from './Ex_Form/FormApp';

function App() {
  return (
    <div className="App">
      {/* <ShoeShop /> */}
      <FormApp />
    </div>
  );
}

export default App;
