import React, { Component } from 'react'

export default class ProductItem extends Component {
    render() {
        return (
            <div className='col-4'>
                <div className=''>
                    <img style={{ width: '90%' }} src={this.props.item.image} alt="" />
                    <div className='cart-body'>
                        <h5>{this.props.item.name}</h5>
                        <p>{this.props.item.price}</p>
                        <div>
                            <button
                                className='btn btn-success mr-2'
                                onClick={() => { this.props.addToCart(this.props.item) }}>
                                Add to Cart
                            </button>
                            <button className='btn btn-primary'
                                onClick={() => { this.props.setStateModal(this.props.item) }}>
                                View detail
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
