import React, { Component } from 'react'
import ProductItem from './ProductItem'

export default class ProductList extends Component {
    renderList() {
        return this.props.productData.map((item, index) => {
            return (
                <ProductItem
                    key={index}
                    item={item}
                    addToCart={this.props.addToCart}
                    setStateModal={this.props.setStateModal}
                />
            )
        })
    }
    render() {
        return (
            <div className='row'>
                {this.renderList()}
            </div>
        )
    }
}
