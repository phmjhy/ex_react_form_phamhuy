import React, { Component } from 'react'
import UserForm from './UserForm'
import UserTable from './UserTable'

export default class FormApp extends Component {
    state = {
        userList: [{
            id: 1,
            name: 'alice',
            password: '11'
        }]
    }
    handleUserAdd = (newUser) => {
        let cloneUserList = [...this.state.userList, newUser]
        // this.setState({ userList: cloneUserList })
    }
    render() {
        return (
            <div>
                <h2 className='bg-dark text-light text-light py-2'>Form đăng ký</h2>
                <UserForm handleUserAdd={this.handleUserAdd} />
                <UserTable userList={this.state.userList} />

            </div>
        )
    }
}
