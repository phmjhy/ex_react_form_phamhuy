import React, { Component } from 'react'

export default class UserForm extends Component {
    constructor(props) {
        super(props)
        this.inputRef = React.createRef()
    }
    state = {
        user: {
            name: '',
            password: '',
            email: '',
            hoTen: '',
            sdt: '',
        }
    }

    // đánh số thứ tự cho list
    handleUserSubmit = () => {
        let newUser = this.state.user
        newUser.id = nanoid(5);
        this.props.handleUserAdd(newUser);
    }

    // truyền giá trị vào value input
    handleGetUserForm = (e) => {
        console.log(e.target.value)
        let { value, name: key } = e.target
        let cloneUser = { ...this.state.user }
        cloneUser[key] = value;
        this.setState({ user: cloneUser }, () => {
            console.log(this.state.user)
        })
    }

    render() {
        return (
            <div className='container row mx-auto text-left'>
                <div className='col-6'>
                    <div class="form-group">
                        <label htmlFor="" className=''>Tài khoản</label>
                        <input
                            onChange={this.handleGetUserForm}
                            value={this.state.user.name}
                            ref={this.inputRef}
                            type="text"
                            className="form-control"
                            name='name'
                            aria-describedby="helpId"
                            placeholder='' />
                    </div>
                    <div class="form-group">
                        <label htmlFor="" className=''>Mật khẩu</label>
                        <input
                            onChange={this.handleGetUserForm}
                            value={this.state.user.password}
                            type="text"
                            className="form-control"
                            name='password'
                            aria-describedby="helpId"
                            placeholder='' />
                    </div>
                    <div class="form-group">
                        <label htmlFor="" className=''>Email</label>
                        <input
                            value={this.state.user.email}
                            type="text"
                            className="form-control"
                            name
                            aria-describedby="helpId"
                            placeholder='' />
                    </div>
                </div>
                <div className='col-6'>
                    <div class="form-group">
                        <label htmlFor="" className=''>Họ tên</label>
                        <input
                            value={this.state.user.hoTen}
                            type="text"
                            className="form-control"
                            name
                            aria-describedby="helpId"
                            placeholder='' />
                    </div>
                    <div class="form-group">
                        <label htmlFor="" className=''>Số điện thoại</label>
                        <input
                            value={this.state.user.sdt}
                            type="text"
                            className="form-control"
                            name
                            aria-describedby="helpId"
                            placeholder='' />
                    </div>
                    <div class="form-group">
                        <label htmlFor="" className=''>Mã loại người dùng</label>
                        <select name="" id="">
                            <option value="Khách hàng">Khách hàng</option>
                        </select>
                    </div>
                </div>
                <button onClick={this.handleUserAdd} className='btn btn-success mx-3'>Đăng ký</button>
                <button className='btn btn-primary'>Cập nhật</button>
            </div>
        )
    }
}
