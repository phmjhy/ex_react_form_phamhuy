import React, { Component } from 'react'

export default class UserTable extends Component {
    renderUserTable = () => {
        return this.props.userList.map((item, key) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.password}</td>
                    <td>
                        <button className='btn btn-danger mr-2'>Delete</button>
                        <button className='btn btn-waring'>Edit</button>
                    </td>
                </tr>
            )
        })
    }
    render() {
        return (
            <table className='table'>
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Name</td>
                        <td>Password</td>

                    </tr>
                </thead>
            </table>
        )
    }
}
